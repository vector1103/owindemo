﻿using Owin;

namespace OwinNetDemo
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Run(context => context.Response.WriteAsync("Hello OWIN from Net!"));
        }
    }
}
