﻿using Microsoft.Owin.Hosting;
using System;

namespace OwinNetDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            using (WebApp.Start<Startup>("http://localhost:9000"))
            {
                Console.WriteLine("Server is started!");
                Console.WriteLine("Press any key to terminate!");
                Console.ReadKey();
            }
        }
    }
}
