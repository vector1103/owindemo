﻿using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using System.Threading.Tasks;

namespace OwinCoreDemo
{
    public class BenchMiddleware
    {
        private readonly RequestDelegate _next;

        public BenchMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var watch = Stopwatch.StartNew();

            context.Response.OnStarting(state =>
            {
                var elapsed = watch.Elapsed;
                ((HttpContext)state).Response.Headers.Add("ElapsedTime", elapsed.ToString("c"));
                return Task.CompletedTask;
            }, context);

            await _next(context);
        }
    }
}
